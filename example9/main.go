// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./closures"
	"fmt"
)

func main() {
	// test of closures
	pos, neg := closures.Adder(), closures.Adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}

}
