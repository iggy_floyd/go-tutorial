// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package closures

// Adder returns the function closure  which performs addition
func Adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}
