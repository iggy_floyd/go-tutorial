# closures
--
    import "."

illustrates how to use function closures

## Usage

#### func  Adder

```go
func Adder() func(int) int
```
Adder returns the function closure which performs addition
