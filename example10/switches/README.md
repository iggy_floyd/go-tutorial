# switches
--
    import "."

illustrates how to use 'switch' in the go

## Usage

#### func  GetSystemName

```go
func GetSystemName() string
```
GetSystemName returns the system name

#### func  LogIfStatement

```go
func LogIfStatement() string
```

    LogIfStatement  uses switch without a condition which is the same as switch true.
This construct can be a clean way to write long if-then-else chains.
