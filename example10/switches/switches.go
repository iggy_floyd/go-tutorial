// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package switches

import (
	"runtime"
	"time"
)

// GetSystemName returns the system name
func GetSystemName() string {
	switch os := runtime.GOOS; os {
	case "darwin":
		return "OS X"
	case "linux":
		return "Linux"
	default:
		return os
	}

}

//  LogIfStatement  uses switch without a condition which is the same as switch true.
// This construct can be a clean way to write long if-then-else chains.
func LogIfStatement() string {

	t := time.Now()
	switch {
	case t.Hour() < 12:
		return "Good morning!"
	case t.Hour() < 17:
		return "Good afternoon."
	default:
		return "Good evening."
	}

}
