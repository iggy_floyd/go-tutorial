// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./switches"
	"fmt"
)

func main() {
	fmt.Printf("%s.\n", switches.GetSystemName())
	fmt.Printf("%s.\n", switches.LogIfStatement())

}
