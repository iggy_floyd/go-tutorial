// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

// This is the documentation of the go-tutorial
//
// Introduction
//
// go-tutorial is a simple package illustrating basic 
// features of the go programming
//
// To view this godoc on your own machine, just run
//
//   go get code.google.com/p/go.tools/cmd/godoc
//   go get -u bitbucket.org/iggy_floyd/go-tutorial/  (this repo is made to be public, to allow such "go-getting" )
//   godoc -http=:8080
//
// Then you can open documentation in the browser:
//
//  http://localhost:8080/pkg/bitbucket.org/iggy_floyd/go-tutorial/
package main
