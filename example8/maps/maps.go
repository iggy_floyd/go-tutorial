// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package maps

type Vertex struct {
	Lat, Long float64
}

// m is a map which should created with the make function
var (
	M map[string]Vertex
)

// CreateMap returns a map
func CreateMap() map[string]Vertex {

	var k map[string]Vertex = make(map[string]Vertex)
	k["test"] = Vertex{1., 2.}
	k["test2"] = Vertex{3., 5.}
	return k

}

// CreateMap2 returns a map initialized by a literal
func CreateMapLiteral() map[string]Vertex {

	return map[string]Vertex{
		"test": {
			40.68433, -74.39967,
		},
		"test2": {
			37.42202, -122.08408,
		},
	}

}

// TestMutableMap performs different operations on maps
func TestMutableMap() bool {

	m := make(map[string]int)

	m["Answer"] = 42

	m["Answer"] = 48

	delete(m, "Answer")

	_, ok := m["Answer"]

	return ok

}
