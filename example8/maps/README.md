# maps
--
    import "."

illustrates how to use maps

## Usage

```go
var (
	M map[string]Vertex
)
```
m is a map which should created with the make function

#### func  CreateMap

```go
func CreateMap() map[string]Vertex
```
CreateMap returns a map

#### func  CreateMapLiteral

```go
func CreateMapLiteral() map[string]Vertex
```
CreateMap2 returns a map initialized by a literal

#### func  TestMutableMap

```go
func TestMutableMap() bool
```
TestMutableMap performs different operations on maps

#### type Vertex

```go
type Vertex struct {
	Lat, Long float64
}
```
