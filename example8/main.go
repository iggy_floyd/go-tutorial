// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./maps"
	"fmt"
)

func main() {
	// test of maps
	maps.M = maps.CreateMap()
	fmt.Println(maps.M)
	fmt.Println(maps.CreateMapLiteral())
	fmt.Println(maps.TestMutableMap())

}
