// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./structs"
	"fmt"
)

func main() {

	fmt.Println(structs.Init(1, 2))
	v := structs.Vertex{1, 2}
	k := &v
	fmt.Println(structs.Field(v))
	// structs.Pointer change Vertex.Y to -3
	fmt.Println(structs.Pointer(k))
	fmt.Println(v)

	fmt.Println(structs.P, structs.Q, structs.R, structs.S)
	fmt.Println(structs.New())

}
