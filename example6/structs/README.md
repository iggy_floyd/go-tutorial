# structs
--
    import "."

illustrates how to use structures

## Usage

```go
var (
	P = Vertex{1, 2}  // has type Vertex
	Q = &Vertex{1, 2} // has type *Vertex
	R = &Vertex{X: 1} // Y:0 is implicit
	S = Vertex{}      // X:0 and Y:0

)
```
Public variables show possible literals for structures

#### func  Field

```go
func Field(v Vertex) int
```
Fields demonstrates how to use fields of the structures

#### func  Pointer

```go
func Pointer(v *Vertex) int
```
Pointer function operates with a pointer in go

#### type Vertex

```go
type Vertex struct {
	X int
	Y int
}
```

Vertex is an example of the structures in the go

#### func  Init

```go
func Init(x, y int) Vertex
```
Init performs initialization of the structures

#### func  New

```go
func New() *Vertex
```
New functions uses the expression new(T) which allocates a zeroed T value and
returns a pointer to it.
