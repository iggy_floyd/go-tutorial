// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package structs

// Vertex is an example of the structures in the go
type Vertex struct {
	X int
	Y int
}

// Init performs initialization of the structures
func Init(x, y int) Vertex {
	return Vertex{x, y}
}

//Fields demonstrates how to use fields of the structures
func Field(v Vertex) int {
	k := v
	return k.X

}

//Pointer function operates with a pointer in go
func Pointer(v *Vertex) int {
	k := v
	k.Y = -3
	return k.X

}

// Public variables show  possible literals for structures
var (
	P = Vertex{1, 2}  // has type Vertex
	Q = &Vertex{1, 2} // has type *Vertex
	R = &Vertex{X: 1} // Y:0 is implicit
	S = Vertex{}      // X:0 and Y:0

)

// New functions uses the expression new(T) which allocates a zeroed T value and returns a pointer to it.
func New() *Vertex {

	return new(Vertex)
}
