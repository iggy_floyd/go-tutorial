// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./methods"
	"fmt"
	"math"
)

func main() {
	v := &methods.Vertex{3, 4}
	fmt.Println(v.Abs())
	f := methods.MyFloat(-math.Sqrt2)
	fmt.Println(f.Absfloat())
	fmt.Println(*v)
	v.Scale(0.5)
	fmt.Println(*v)

}
