# methods
--
    import "."

illustrates how to use methods in the go

## Usage

#### type MyFloat

```go
type MyFloat float64
```


#### func (MyFloat) Absfloat

```go
func (f MyFloat) Absfloat() float64
```
Absfloat defines a method of float object

#### type Vertex

```go
type Vertex struct {
	X, Y float64
}
```


#### func (*Vertex) Abs

```go
func (v *Vertex) Abs() float64
```
Abs defines a method of the pointer to a Vertex

#### func (*Vertex) Scale

```go
func (v *Vertex) Scale(f float64)
```
Scale method on the pointer changes an object state
