// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package methods

import "math"

type Vertex struct {
	X, Y float64
}

type MyFloat float64

// Abs defines a method of the pointer to a Vertex
func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Scale method on the pointer changes an object state
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

// Absfloat defines a method of float object
func (f MyFloat) Absfloat() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}
