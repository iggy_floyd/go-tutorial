// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./channels"
	"./multichannels"
	"fmt"
)

func main() {

	// test a sum function
	c := make(chan int)
	a := []int{7, 2, 8, -9, 4, 0}
	go channels.Sum(a[:len(a)/2], c)
	go channels.Sum(a[len(a)/2:], c)
	x, y := <-c, <-c // receive from c

	fmt.Println(x, y, x+y)

	// test a ping function
	message := make(chan string)
	channels.Ping(message)
	msg := <-message
	fmt.Println(msg)

	// test the buffered channel
	channels.TestBufferedChannel()

	// test the "close" and "range" of the buffered channel
	channels.PrintFibonacci()

	// test the synchronisation of the threads
	channels.Master()
	// test the directed channels
	channels.PingPong()

	// test the select operation in multiple communication operations
	multichannels.PrintFibonacci()

	// test timeouts in the go
	c1 := make(chan string)
	multichannels.Sender(c1, 5)
	multichannels.Timeouter(c1, 2) // should print timeout 2
	multichannels.Timeouter(c1, 4) // should print sleep 5

	// test the "default" of the select
	multichannels.TickTack()
}
