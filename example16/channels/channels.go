// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package channels

import (
	"fmt"
	"time"
)

// Sum uses a channel to return a result
func Sum(a []int, c chan int) {
	sum := 0
	for _, v := range a {
		sum += v
	}
	c <- sum // send sum to c
}

// Ping send the "ping" to the channel msg
func Ping(msg chan string) {

	go func() { msg <- "ping" }()

}

// TestBufferedChannel tests buffered channels
func TestBufferedChannel() {
	messages := make(chan string, 2)
	messages <- "buffered"
	messages <- "channel"
	fmt.Println(<-messages)
	fmt.Println(<-messages)
}

// Fibonacci is a function which sends an element of the fibonacci set to the channel.
// It 'closes' channel at the end.
func Fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c)
}

// PrintFibonacci starts goroutining Fibonacci function and ranges over the buffered channel
func PrintFibonacci() {
	c := make(chan int, 10)
	go Fibonacci(cap(c), c) // uses a buffered channel
	for i := range c {
		fmt.Println(i)
	}
}

// This and next functions illustrate the  synchronization of threads unsing channels
// Worker is a goroutine
func Worker(done chan bool) {
	fmt.Print("Worker: working...")
	time.Sleep(time.Second)
	fmt.Println("Worker: ...done")
	done <- true
}

// Master starts Worker
func Master() {
	fmt.Println("Master: working...")
	done := make(chan bool, 1)
	go Worker(done)
	<-done
	fmt.Println("Master: ...done")

}

// This and next functions illustrate the use of directed channels
// ReadIn pushes the message to the channel
func ReadIn(pings chan<- string, msg string) {

	pings <- msg
}

// ReadOut pulls the message from one channel and  pushes it to another channel
func ReadOut(pings <-chan string, pongs chan<- string) {
	msg := <-pings
	pongs <- msg
}

// PingPong plays the "ping-pong" game via ReadIn and ReadOut functions
func PingPong() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ReadIn(pings, "PingPong: passed message")
	ReadOut(pings, pongs)
	fmt.Println(<-pongs)
}
