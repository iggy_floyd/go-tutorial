# channels
--
    import "."

illustrates how to use channels (named pipes) between goroutines (threads) in
the go

## Usage

#### func  Fibonacci

```go
func Fibonacci(n int, c chan int)
```
Fibonacci is a function which sends an element of the fibonacci set to the
channel. It 'closes' channel at the end.

#### func  Master

```go
func Master()
```
Master starts Worker

#### func  Ping

```go
func Ping(msg chan string)
```
Ping send the "ping" to the channel msg

#### func  PingPong

```go
func PingPong()
```
PingPong plays the "ping-pong" game via ReadIn and ReadOut functions

#### func  PrintFibonacci

```go
func PrintFibonacci()
```
PrintFibonacci starts goroutining Fibonacci function and ranges over the
buffered channel

#### func  ReadIn

```go
func ReadIn(pings chan<- string, msg string)
```
This and next functions illustrate the use of directed channels ReadIn pushes
the message to the channel

#### func  ReadOut

```go
func ReadOut(pings <-chan string, pongs chan<- string)
```
ReadOut pulls the message from one channel and pushes it to another channel

#### func  Sum

```go
func Sum(a []int, c chan int)
```
Sum uses a channel to return a result

#### func  TestBufferedChannel

```go
func TestBufferedChannel()
```
TestBufferedChannel tests buffered channels

#### func  Worker

```go
func Worker(done chan bool)
```
This and next functions illustrate the synchronization of threads unsing
channels Worker is a goroutine
