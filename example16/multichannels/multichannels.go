// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package multichannels

import (
	"fmt"
	"time"
)

//  Fibonacci uses "select" to choose operation dependign on the ready status of two channels
func Fibonacci(c chan<- int, quit <-chan int) {
	x, y := 0, 1
	for { // infinite loop
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

// PrintFibonacci starts goroutining Fibonacci function
func PrintFibonacci() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}() // controls the  Fibonacci "select" cases

	Fibonacci(c, quit) // loops until quit <- 0
}

// Sender and Timeouter   functions show realization of the timeouts in the go
// Sender sends something within sec seconds
func Sender(c chan string, sec int64) {

	go func() {
		time.Sleep(time.Second * time.Duration(sec))
		c <- "sleep " + fmt.Sprintf("%d", sec)
	}()

}

// Timeouter waits a message from Sender and after sec seconds drops "timeout" messages
func Timeouter(c chan string, sec int64) {
	select {
	case res := <-c:
		fmt.Println(res)
	case <-time.After(time.Second * time.Duration(sec)):
		fmt.Println("timeout " + fmt.Sprintf("%d", sec))
	}
}

// TickTack demonstrates the non-blocking multiple communication between two threads
func TickTack() {

	tick := time.Tick(100 * time.Millisecond)  // sends to the "tick" channel one tick each second
	boom := time.After(500 * time.Millisecond) // sends to the "boom" channel one tick each  5seconds

	for {
		select {
		case <-tick:
			fmt.Println("tick.")
		case <-boom:
			fmt.Println("BOOM!")
			return
		default:
			fmt.Println("    .tack") // between tick and boom, send "tack"
			time.Sleep(50 * time.Millisecond)
		}
	}

}
