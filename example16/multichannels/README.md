# multichannels
--
    import "."

illustrates how to use channels for multiple communication operations

## Usage

#### func  Fibonacci

```go
func Fibonacci(c chan<- int, quit <-chan int)
```
Fibonacci uses "select" to choose operation dependign on the ready status of two
channels

#### func  PrintFibonacci

```go
func PrintFibonacci()
```
PrintFibonacci starts goroutining Fibonacci function

#### func  Sender

```go
func Sender(c chan string, sec int64)
```
Sender and Timeouter functions show realization of the timeouts in the go Sender
sends something within sec seconds

#### func  TickTack

```go
func TickTack()
```
TickTack demonstrates the non-blocking multiple communication between two
threads

#### func  Timeouter

```go
func Timeouter(c chan string, sec int64)
```
Timeouter waits a message from Sender and after sec seconds drops "timeout"
messages
