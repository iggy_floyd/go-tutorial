// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./loops"
	"fmt"
)

func main() {
	// print the result of the loop
	fmt.Println(loops.ExampleLoop1(1, 4))
	fmt.Println(loops.ExampleLoop2(10))

}
