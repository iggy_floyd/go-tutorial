// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package loops

//ExampleLoop1 shows a 'for' loop in the go
func ExampleLoop1(i, j int) int {
	sum := 0

	for _i := i; _i < j; _i++ {
		sum += _i
	}

	return sum
}

//ExampleLoop2 shows a 'while' loop in the go
func ExampleLoop2(i int) int {
	sum := 1

	for sum < i {
		sum += sum
	}

	return sum
}
