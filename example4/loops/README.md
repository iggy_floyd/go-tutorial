# loops
--
    import "."

illustrates how to use 'for' statements

## Usage

#### func  ExampleLoop1

```go
func ExampleLoop1(i, j int) int
```
ExampleLoop1 shows a 'for' loop in the go

#### func  ExampleLoop2

```go
func ExampleLoop2(i int) int
```
ExampleLoop2 shows a 'while' loop in the go
