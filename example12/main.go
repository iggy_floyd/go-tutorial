// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./interfaces"
	"fmt"
)

// Abser interface will be the same for Vertex and MyFloat
type Pow2 interface {
	Pow2() float64
}

func main() {
	interfaces.TestInterface()
	var inter Pow2
	inter = interfaces.Vertex{4., 3.}
	fmt.Println(inter.Pow2())
}
