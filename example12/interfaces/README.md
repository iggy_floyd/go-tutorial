# interfaces
--
    import "."

illustrates how to use interfaces in the go

## Usage

#### func  TestInterface

```go
func TestInterface()
```
TestInterface function test an abstract interface

#### type Abser

```go
type Abser interface {
	Abs() float64
}
```

Abser interface will be the same for Vertex and MyFloat

#### type MyFloat

```go
type MyFloat float64
```


#### func (MyFloat) Abs

```go
func (f MyFloat) Abs() float64
```

#### func (MyFloat) Pow2

```go
func (f MyFloat) Pow2() float64
```

#### type Vertex

```go
type Vertex struct {
	X, Y float64
}
```


#### func (Vertex) Abs

```go
func (v Vertex) Abs() float64
```
implementation of the interface Abser

#### func (Vertex) Pow2

```go
func (v Vertex) Pow2() float64
```
implementation of the interface Pow2
