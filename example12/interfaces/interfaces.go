// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package interfaces

import (
	"fmt"
	"math"
)

// Abser interface will be the same for Vertex and MyFloat
type Abser interface {
	Abs() float64
}

//TestInterface function test an abstract interface
func TestInterface() {
	var a Abser
	f := MyFloat(-math.Sqrt2)
	a = f
	fmt.Println(a.Abs())
	v := Vertex{3, 4}
	a = v
	fmt.Println(a.Abs())
}

type Vertex struct {
	X, Y float64
}

type MyFloat float64

// implementation of the interface Abser
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

// implementation of the interface Pow2
func (v Vertex) Pow2() float64 {
	return v.X * v.X
}

func (f MyFloat) Pow2() float64 {
	return float64(f * f)
}
