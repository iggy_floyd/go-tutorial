// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./functions"
	"./variadic"
	"fmt"
)

func main() {
	// here the way how to use the function add
	fmt.Println(functions.Add(42, 13))

	// swaping of two worlds
	a, b := functions.Swap("hello", "world")
	fmt.Println(a, b)

	// an example of a named returned result
	fmt.Println(functions.Split(21))

	// an example of a function with variadic arguments
	nums := []int{1, 2, 3, 4, 5}
	fmt.Println(variadic.Sum(nums...))
}
