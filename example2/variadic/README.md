# variadic
--
    import "."

illustrates how to use variadic functions

## Usage

#### func  Sum

```go
func Sum(nums ...int) int
```
Sum function sums variant number of input parameters
