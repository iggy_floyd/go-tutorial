// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package variadic

// Sum function sums variant number of input parameters
func Sum(nums ...int) int {

	total := 0
	for _, num := range nums {
		total += num
	}
	return total
}
