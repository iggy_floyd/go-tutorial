// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package functions

// Add function is an example of functions in go
func Add(x, y int) int {
	return x + y
}

// Swap function is an example of a function with  multiple returned values
func Swap(x, y string) (string, string) {
	return y, x
}

// Split function returns named multiple arguments
// pay an attention on (sum int) and 'empty' return statement

func Split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}
