# functions
--
    import "."

illustrates how to use functions

## Usage

#### func  Add

```go
func Add(x, y int) int
```
Add function is an example of functions in go

#### func  Split

```go
func Split(sum int) (x, y int)
```

#### func  Swap

```go
func Swap(x, y string) (string, string)
```
Swap function is an example of a function with multiple returned values
