# goroutines
--
    import "."

illustrates how to use concurency patterns in the go

## Usage

#### func  F

```go
func F(from string)
```
F is a simple function

#### func  Say

```go
func Say(s string)
```
Say is a function with a delay of 3 seconds
