// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package goroutines

import (
	"fmt"
	"time"
)

// F is a simple function
func F(from string) {
	for i := 0; i < 300; i++ {
		fmt.Println(from, ":", i)
	}
}

// Say is a function with a delay of 3 seconds
func Say(s string) {
	for i := 0; i < 500; i++ {
		time.Sleep(50 * time.Millisecond)
		fmt.Println(s)
	}
}
