// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./goroutines"
	"fmt"
)

func main() {

	// go starts a lightweight thread
	go goroutines.Say("world")
	goroutines.Say("hello")

	goroutines.F("direct")
	go goroutines.F("goroutine")

	// start go thread using the function closure
	go func(msg string) {
		fmt.Println(msg)
	}("going")

	var input string
	fmt.Scanln(&input)
	fmt.Println("done")
}
