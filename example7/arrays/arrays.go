// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package arrays

import "fmt"

// Public variables demonstrates declarations of arrays
var (
	A [2]string // array
	S []string  // slice

)

//SetArray sets an input array
func SetArray(a *[2]string) *[2]string {

	a[0] = "Hello"
	a[1] = "World"

	return a

}

// GetSlice returns a slice
func GetSlice() []int {

	return []int{2, 3, 5, 7, 11, 13}
}

// GetEmptySlice returns an empty slice with cap==5
func GetEmptySlice() []int {

	return make([]int, 0, 5)
}

// TestNil tests the empty (nil) slice
func TestNil(s []string) bool {

	return s == nil
}

// GetRange return slice (range) of the slice
func GetRange(slice []int, start, end int) []int {

	res := make([]int, len(slice), cap(slice))
	for i, v := range slice {
		if i > start && i < end {
			fmt.Printf("%d\n", v)
			res[i] = v
		}
	}

	return res
}
