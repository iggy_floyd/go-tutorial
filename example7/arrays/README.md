# arrays
--
    import "."

illustrates how to use arrays

## Usage

```go
var (
	A [2]string // array
	S []string  // slice

)
```
Public variables demonstrates declarations of arrays

#### func  GetEmptySlice

```go
func GetEmptySlice() []int
```
GetEmptySlice returns an empty slice with cap==5

#### func  GetRange

```go
func GetRange(slice []int, start, end int) []int
```
GetRange return slice (range) of the slice

#### func  GetSlice

```go
func GetSlice() []int
```
GetSlice returns a slice

#### func  SetArray

```go
func SetArray(a *[2]string) *[2]string
```
SetArray sets an input array

#### func  TestNil

```go
func TestNil(s []string) bool
```
TestNil tests the empty (nil) slice
