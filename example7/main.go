// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./arrays"
	"fmt"
)

func main() {
	// test of arrays
	a := &arrays.A
	fmt.Println(*arrays.SetArray(a))

	// test of slices
	s := arrays.GetSlice()
	fmt.Println("s ==", s)

	p := arrays.GetEmptySlice()
	fmt.Println("p ==", p)

	fmt.Printf("p len=%d cap=%d %v\n", len(p), cap(p), p)

	fmt.Println(arrays.TestNil(arrays.S))

	fmt.Println(arrays.GetRange(s, 2, 4))

}
