// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./ifs"
	"fmt"
)

func main() {
	// print the result of the Sqrt
	fmt.Println(ifs.Sqrt(4), ifs.Sqrt(-4))

	// print results of the Pow
	fmt.Println(ifs.Pow(3, 2, 10), ifs.Pow(3, 3, 10))

}
