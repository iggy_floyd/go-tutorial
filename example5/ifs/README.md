# ifs
--
    import "."

illustrates how to use 'if' statements

## Usage

#### func  Pow

```go
func Pow(x, n, lim float64) float64
```
Pow shows 'if' with a short statement and else

#### func  Sqrt

```go
func Sqrt(x float64) string
```
Sqrt ilustrates an example of 'if' use
