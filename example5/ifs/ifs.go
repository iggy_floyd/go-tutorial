// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package ifs

import (
	"fmt"
	"math"
)

// Sqrt ilustrates an example of 'if' use
func Sqrt(x float64) string {
	if x < 0 {
		return fmt.Sprint(math.Sqrt(-x)) + "i"
	}
	return fmt.Sprint(math.Sqrt(x))
}

// Pow shows 'if' with a short statement and else
func Pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	} else {
		fmt.Printf("%g >= %g\n", v, lim)
	}
	return lim
}
