// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./variables"
	"fmt"
)

func main() {
	// print  the variables
	fmt.Println(variables.I, variables.C, variables.Python, variables.Java)
	fmt.Println(variables.K, variables.J, variables.C2, variables.Python2, variables.Java2)
	fmt.Println(variables.Short())
	variables.Types()
	variables.TypesConversation()
	fmt.Println(variables.Pi)
	fmt.Println(variables.NeedInt(variables.Small))
	fmt.Println(variables.NeedFloat(variables.Small))
	fmt.Println(variables.NeedFloat(variables.Big))
}
