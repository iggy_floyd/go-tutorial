// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package variables

import (
	"fmt"
	"math/cmplx"
)

// Public variables w/o initializers
var I int
var C, Python, Java bool

// Public variables w/ initializers
var K, J int = 1, 2
var C2, Python2, Java2 = true, false, "no!"

// Short function uses short assignment statement
func Short() (int, bool, bool, string) {
	k := 3
	c, python, java := true, false, "no!"
	return k, c, python, java
}

// Basic types of the go
var (
	ToBe   bool       = false
	MaxInt uint64     = 1<<64 - 1
	Z      complex128 = cmplx.Sqrt(-5 + 12i)
)

//Types prints basic types
func Types() {

	const f = "%T -- (%v)\n"
	fmt.Printf(f, ToBe, ToBe)
	fmt.Printf(f, MaxInt, MaxInt)
	fmt.Printf(f, Z, Z)

}

// TypesConversation shows types conversions
func TypesConversation() {
	i := 42
	fl := float64(i)
	u := uint(fl)
	const f = "%T -- (%v)\n"
	fmt.Printf(f, i, i)
	fmt.Printf(f, fl, fl)
	fmt.Printf(f, u, u)
}

// Pi is a constant in the go
const Pi = 3.14

// Numeric constants
const (
	Big   = 1 << 100
	Small = Big >> 99
)

// Functions needed to test numeric constants
func NeedInt(x int) int { return x*10 + 1 }
func NeedFloat(x float64) float64 {
	return x * 0.1
}
