# variables
--
    import "."

illustrates how to use variables

## Usage

```go
const (
	Big   = 1 << 100
	Small = Big >> 99
)
```
Numeric constants

```go
const Pi = 3.14
```
Pi is a constant in the go

```go
var (
	ToBe   bool       = false
	MaxInt uint64     = 1<<64 - 1
	Z      complex128 = cmplx.Sqrt(-5 + 12i)
)
```
Basic types of the go

```go
var C, Python, Java bool
```

```go
var C2, Python2, Java2 = true, false, "no!"
```

```go
var I int
```
Public variables w/o initializers

```go
var K, J int = 1, 2
```
Public variables w/ initializers

#### func  NeedFloat

```go
func NeedFloat(x float64) float64
```

#### func  NeedInt

```go
func NeedInt(x int) int
```
Functions needed to test numeric constants

#### func  Short

```go
func Short() (int, bool, bool, string)
```
Short function uses short assignment statement

#### func  Types

```go
func Types()
```
Types prints basic types

#### func  TypesConversation

```go
func TypesConversation()
```
TypesConversation shows types conversions
