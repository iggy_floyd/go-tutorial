// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package https

import (
	"fmt"
	"net/http"
)

//Simple "hello world" handler
type Hello struct{}

// interface of ServeHTTP is defined in "net/http"
func (h Hello) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request) {
	fmt.Fprint(w, "Hello!")
}

// Complex srtuctures to handle several http routes
type String string

type Struct struct {
	Greeting string
	Punct    string
	Who      string
}

// interface of ServeHTTP is defined in "net/http"
func (s String) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request) {
	fmt.Fprint(w, s)
}

// interface of ServeHTTP is defined in "net/http"
func (s Struct) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request) {
	fmt.Fprint(w, s.Greeting, s.Punct, s.Who)
}
