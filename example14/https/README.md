# https
--
    import "."

illustrates how to use a http server in the go

## Usage

#### type Hello

```go
type Hello struct{}
```

Simple "hello world" handler

#### func (Hello) ServeHTTP

```go
func (h Hello) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request)
```
interface of ServeHTTP is defined in "net/http"

#### type String

```go
type String string
```

Complex srtuctures to handle several http routes

#### func (String) ServeHTTP

```go
func (s String) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request)
```
interface of ServeHTTP is defined in "net/http"

#### type Struct

```go
type Struct struct {
	Greeting string
	Punct    string
	Who      string
}
```


#### func (Struct) ServeHTTP

```go
func (s Struct) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request)
```
interface of ServeHTTP is defined in "net/http"
