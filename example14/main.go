// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./https"
	"net/http"
)

func main() {

	// settle up routes
	http.Handle("/string", https.String("I'm a frayed knot."))
	http.Handle("/struct", &https.Struct{"Hello", ":", "Gophers!"})

	http.ListenAndServe("localhost:4000", nil)
	// or use simple hello world responder
	/*
	   var h https.Hello
	   	http.ListenAndServe("localhost:4000", h)
	*/
}
