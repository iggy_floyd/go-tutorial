# myerrors
--
    import "."

illustrates how to use errors in the go

## Usage

#### func  F1

```go
func F1(arg int) (int, error)
```
F1 shows how to work out the standart error method

#### func  F2

```go
func F2(arg int) (int, error)
```
F2 shows how to work out the user-defined error method

#### type MyError

```go
type MyError struct {
	When time.Time
	What string
}
```


#### func (*MyError) Error

```go
func (e *MyError) Error() string
```
Error is a basic method of the interface error
