// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package myerrors

import (
	"errors"
	"fmt"
	"time"
)

type MyError struct {
	When time.Time
	What string
}

// Error is a basic method of the interface error
func (e *MyError) Error() string {
	return fmt.Sprintf("at %v, %s", e.When, e.What)
}

// F1 shows how to work out the standart error method
func F1(arg int) (int, error) {
	if arg == 42 {

		return -1, errors.New("can't work with 42")

	}

	return arg + 3, nil
}

// F2 shows how to work out the user-defined error method
func F2(arg int) (int, error) {
	if arg == 42 {

		return -1, &MyError{time.Now(), "can't work with 42"}

	}

	return arg + 3, nil
}
