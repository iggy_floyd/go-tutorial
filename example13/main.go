// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./myerrors"
	"fmt"
)

func main() {

	// using standart errors object
	for _, i := range []int{7, 42} {
		if r, e := myerrors.F1(i); e != nil {
			fmt.Println("F1 failed:", e)
		} else {
			fmt.Println("F1 worked:", r)
		}
	}

	//using user-defined error object
	for _, i := range []int{7, 42} {
		if r, e := myerrors.F2(i); e != nil {
			fmt.Println("F2 failed:", e)
		} else {
			fmt.Println("F2 worked:", r)
		}
	}

	// the way how to converse the 'error' object to the MyError object
	_, e := myerrors.F2(42)
	if ae, ok := e.(*myerrors.MyError); ok {
		fmt.Println(ae.When)
		fmt.Println(ae.What)
	}

}
